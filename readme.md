# Overview
We're looking for a full-stack developer who enjoys all aspects of software development in a predominantly web-oriented setting.  Our applications consist of RESTful APIs and standalone clients which means that the right person for this role is a solid developer in both areas.

Like any other development team, we have our preferred libraries and tools which are listed below.  If you're not used to these particular items but are comfortable with the underlying technologies, don't worry about fully implementing every feature of every tool.  We use Javascript on the client and Java on the server.  If you can work in these two areas, we're willing to allow time for picking up the libraries.

# Submitting this App
Please provide a link to your signup page and a link to your repository with your emailed resume.  You may fork this repo to get started or you may host elsewhere.  If hosting outside Bitbucket, please use a well-known, reputable repository host.  Links to custom, privately-hosted repositories will not be accepted.

# To-Do App
Palmetto Health developers are great but they're ~~a bit~~ totally scatter-brained.  A to-do list application would really go a long way in getting things done.  They'd probably do a lot better with a cloud-based tool that would allow them to:

* Create tasks consisting of
    * Up to 500 characters
    * Due date/time
* View all tasks and due dates
* Delete tasks
* Edit tasks
* Sign up with an email address and password
* Perform password resets when needed
* All tasks should be associated with the user who created them and should only be visible to that user.
* All tasks must be persisted in a relational database of your choice.

_This application will only be used for candidate assessment and will not be put into production at Palmetto Health._

# Requirements
## Server/API
1. The server API should be written in Java and deployed to a Wildfly application server.
    * There are a few different options for doing this but Red Hat's [OpenShift](https://www.openshift.com/) is free and more than capable of handling this type of application.  It would certainly be commendable to start from a vanilla VM but we're not necessarily looking for server admin skills.  That said, more power to you if you go that route.
2. The server API should use Spring REST for handling HTTP requests and communicate exclusively in JSON (no XML or other formats).  Use Spring's included marshaling features and do not include any custom marshaling libraries or hand-write POJO conversion utilities.
3. Any relational database will do for persistence.  If you use OpenShift, MySQL can be added and configured into your Wildfly instance with a few clicks.
4. Database calls should be made using JNDI-listed datasources.
5. All database queries should be made using stored procedures.

## Client
1. The only client required for this application is a browser-based, Javascript, single-page application.
2. Use of the latest* Angular JS v1.x.x is required.
    * All routing should be done using Angular's routing service and URL hashes.
    * All HTTP requests should use Angular's $http promise-based service.
3. Use of the latest* [Angular Material](https://material.angularjs.org/latest/) for the UI is required.
    * The UI should be fully responsive and all app operations should be available across all screen sizes.

*The latest version of Angular may not be compatible with Angular Material or vice versa as Angular Material's release schedule is pretty aggressive.  Just use whichever two versions are the latest compatible options.  If using Bower, you can simply include the latest Angular Material and it'll handle fishing out the correct Angular version.

## Authentication
1. Do not write this yourself.  Any submissions containing authentication implementations will be rejected.
2. There are a few different free third-party resources available for authentication ([Auth0](https://auth0.com/), [Stormpath](https://stormpath.com/), [Firebase](https://firebase.google.com/)).  All authentication and authorization _must_ be tokenized (see next item).
3. All authentication/authorization should be tokenized with JWTs.  API access should be granted exclusively via a JWT.  No server-side sessions should be used in this application.

# Problem Set
Within your to-do list app, you'll need to include the solutions to the problems listed below.  This doesn't have anything to do with the app itself - these are just standalone questions we'd like to discuss during an interview.  As for the front end, feel free to combine these problems into a separate page but be sure your Java solution deploys with the rest of your To-Do app's API.

## Javascript
Write a single function which prints the numbers 1 through 10 to the console in descending order.  Just before your entire script has finished executing, the string "Done" should be printed to the console.  This function:

1. May only take a single argument.
2. May _NOT_ perform subtraction or any other action which decreases any value.
3. May _NOT_ make use of any negative numbers or representations thereof in any alternate encoding.
4. May _NOT_ iterate through a list or any other data structure.

## Java
Yep..  It's FizzBuzz time!  You know the rules: Write a Java API endpoint which prints a list of numbers from 1 to 100.  If a number in the list is divisible by three, replace that number with "Fizz".  If a number in the list is divisible by five, replace that number with "Buzz".  If a number in the list is divisible by both three _and_ five, replace that number with "FizzBuzz".

Like all other endpoints in your application, this should return a JSON array.  If you'd like to create a front end to render this, please feel free.  Just make sure to include instructions for us to navigate to that page.

Unlike all other endpoints in your application, this endpoint should be publicly available.  Feel free to include any anti-spambot mechanisms you need but the user shouldn't have to sign in to see the results of FizzBuzz.